package handlers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"

	"github.com/netbucket/pet-rescue/datastore"
)

const filepath = "../testdata/shelters.json"

var r http.Handler

func init() {
	ds := new(datastore.FileDatastore)

	err := ds.Open(filepath)

	if err != nil {
		log.Fatal(err)
	}

	r = AssignHandlers(ds)
}

func TestListShelters(t *testing.T) {
	req, err := http.NewRequest("GET", "/shelters/", nil)

	if err != nil {
		t.Fatal(err)
	}

	rec := httptest.NewRecorder()

	r.ServeHTTP(rec, req)

	if rec.Code != http.StatusOK {
		t.Fatalf("Expected HTTP status %d, got %d", http.StatusOK, rec.Code)
	}

	data := struct {
		Shelters []datastore.ShelterSummary `json:"data"`
	}{}

	if err := json.Unmarshal(rec.Body.Bytes(), &data); err != nil {
		t.Error(err)
		t.Fatal(rec.Body)
	}

	if len(data.Shelters) == 0 {
		t.Fatal("Received no shelter data")
	}
}

func TestShelterDetails(t *testing.T) {
	targetID := 10241

	req, err := http.NewRequest("GET", fmt.Sprintf("/shelters/%d", targetID), nil)

	if err != nil {
		t.Fatal(err)
	}

	rec := httptest.NewRecorder()

	r.ServeHTTP(rec, req)

	if rec.Code != http.StatusOK {
		t.Fatalf("Expected HTTP status %d, got %d", http.StatusOK, rec.Code)
	}

	data := struct {
		Shelter datastore.Shelter `json:"data"`
	}{}

	if err := json.Unmarshal(rec.Body.Bytes(), &data); err != nil {
		t.Error(err)
		t.Fatal(rec.Body)
	}

	if data.Shelter.ID != targetID {
		t.Fatalf("Shelter ID mismatch: got %d, expected %d", data.Shelter.ID, targetID)
	}
}

func TestShelterDetailsBatch(t *testing.T) {
	targetIDs := []int{10241, 40964}

	var buffer bytes.Buffer

	buffer.WriteString("/shelters/")

	for idx, ID := range targetIDs {
		if idx > 0 {
			buffer.WriteString(",")
		}

		buffer.WriteString(strconv.Itoa(ID))
	}
	req, err := http.NewRequest("GET", buffer.String(), nil)

	if err != nil {
		t.Fatal(err)
	}

	rec := httptest.NewRecorder()

	r.ServeHTTP(rec, req)

	if rec.Code != http.StatusOK {
		t.Fatalf("Expected HTTP status %d, got %d", http.StatusOK, rec.Code)
	}

	data := struct {
		Shelters []datastore.Shelter `json:"data"`
	}{}

	if err := json.Unmarshal(rec.Body.Bytes(), &data); err != nil {
		t.Error(err)
		t.Fatal(rec.Body)
	}

	if len(data.Shelters) != len(targetIDs) {
		t.Fatalf("Got %d shelters, expected %d", len(data.Shelters), len(targetIDs))
	}
}

func TestShelterDetailsBatchNotFound(t *testing.T) {
	req, err := http.NewRequest("GET", "/shelters/0,99", nil)

	if err != nil {
		t.Fatal(err)
	}

	rec := httptest.NewRecorder()

	r.ServeHTTP(rec, req)

	if rec.Code != http.StatusNotFound {
		t.Fatalf("Expected HTTP status %d, got %d", http.StatusNotFound, rec.Code)
	}
}

func TestShelterNotFound(t *testing.T) {
	targetID := 0

	req, err := http.NewRequest("GET", fmt.Sprintf("/shelters/%d", targetID), nil)

	if err != nil {
		t.Fatal(err)
	}

	rec := httptest.NewRecorder()

	r.ServeHTTP(rec, req)

	if rec.Code != http.StatusNotFound {
		t.Fatalf("Expected HTTP status %d, got %d", http.StatusNotFound, rec.Code)
	}
}

func TestBadShelterId(t *testing.T) {
	req, err := http.NewRequest("GET", "/shelters/z0d", nil)

	if err != nil {
		t.Fatal(err)
	}

	rec := httptest.NewRecorder()

	r.ServeHTTP(rec, req)

	if rec.Code != http.StatusBadRequest {
		t.Fatalf("Expected HTTP status %d, got %d", http.StatusBadRequest, rec.Code)
	}
}
