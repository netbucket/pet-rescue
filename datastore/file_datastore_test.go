package datastore

import (
	"testing"
)

const filepath = "../testdata/shelters.json"

func TestOpenSucces(t *testing.T) {
	d := openDatastore(t)

	if len(d.model) == 0 {
		t.Fatalf("No data loaded from %s", filepath)
	}
}

func TestOpenFailure(t *testing.T) {
	nofile := "nosuchfile.json"

	d := FileDatastore{}

	err := d.Open(nofile)

	if err == nil {
		t.Fatalf("Should not be able to open %s", nofile)
	}
}

func TestListShelters(t *testing.T) {
	d := openDatastore(t)

	result, err := d.ListShelters()

	if err != nil {
		t.Fatal(err)
	}

	if len(result) == 0 {
		t.Fatal("Unexpected empty result")
	}
}

func TestFindShelterMatch(t *testing.T) {
	d := openDatastore(t)

	var testset = []struct {
		ID            int
		matchExpected bool
	}{
		{10241, true},
		{20482, true},
		{30723, true},
		{40964, true},
		{0, false},
		{1024, false}}

	for _, test := range testset {
		shelter, exists, err := d.FindShelter(test.ID)

		if err != nil {
			t.Fatal(err)
		}

		// If a match is expected, verify that the IDs match
		if test.matchExpected && test.ID != shelter.ID {
			t.Errorf("Expected shelter ID of %d, got %d", test.ID, shelter.ID)
		}

		if test.matchExpected != exists {
			t.Errorf(
				"Test for ID %d failed: match expected: %t, match found: %t",
				test.ID,
				test.matchExpected,
				exists)
		}
	}
}

func TestFindSheltersFullMatch(t *testing.T) {
	d := openDatastore(t)

	keys := []int{10241, 40964}

	result, err := d.FindShelters(keys)

	if err != nil {
		t.Fatal(err)
	}

	if len(keys) != len(result) {
		t.Fatalf("Expected %d matches, got %d", len(keys), len(result))
	}
}

func TestFindSheltersPartialMatch(t *testing.T) {
	d := openDatastore(t)

	keys := []int{10241, 0}

	result, err := d.FindShelters(keys)

	if err != nil {
		t.Fatal(err)
	}

	if len(result) > 1 {
		t.Fatalf("Expected 1 match, got %d", len(result))
	}
}

func TestFindSheltersNoMatch(t *testing.T) {
	d := openDatastore(t)

	keys := []int{0, 1}

	result, err := d.FindShelters(keys)

	if err != nil {
		t.Fatal(err)
	}

	if len(result) > 0 {
		t.Fatalf("Expected no match, got %d", len(result))
	}
}

func TestClose(t *testing.T) {
	d := openDatastore(t)

	err := d.Close()

	if err != nil {
		t.Fatal(err)
	}
}

func openDatastore(t *testing.T) *FileDatastore {
	d := FileDatastore{}

	err := d.Open(filepath)

	if err != nil {
		t.Fatalf("Unable to open %s", filepath)
	}

	return &d
}
