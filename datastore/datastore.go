// Package datastore contains data/persistence related code
package datastore

// Datastore interface governing all datastore implementations
type Datastore interface {
	Open(config string) error
	ListShelters() ([]ShelterSummary, error)
	FindShelter(ID int) (Shelter, bool, error)
	FindShelters(IDs []int) ([]Shelter, error)
	Close() error
}
