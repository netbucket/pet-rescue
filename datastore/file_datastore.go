package datastore

import (
	"encoding/json"
	"io/ioutil"
)

// FileDatastore implements a JSON file based datastore
type FileDatastore struct {
	model map[int]Shelter
}

// Open the datastore. The config string is expected to represent
// a file path to a JSON data set.
func (f *FileDatastore) Open(config string) error {
	raw, err := ioutil.ReadFile(config)

	if err != nil {
		return err
	}

	return json.Unmarshal(raw, &f.model)
}

// ListShelters returns a list of all available shelters
func (f *FileDatastore) ListShelters() ([]ShelterSummary, error) {
	result := make([]ShelterSummary, len(f.model))
	idx := 0

	for _, shelter := range f.model {
		result[idx] = ShelterSummary{shelter.ID, shelter.Name}
		idx++
	}

	return result, nil
}

// FindShelter retrieves shelter details given a shelter ID
func (f *FileDatastore) FindShelter(ID int) (Shelter, bool, error) {
	result, exists := f.model[ID]

	return result, exists, nil
}

// FindShelters retrieves matching shelter details for a list of shelter IDs
func (f *FileDatastore) FindShelters(IDs []int) ([]Shelter, error) {

	result := []Shelter{}

	for _, ID := range IDs {
		shelter, exists := f.model[ID]

		if exists {
			result = append(result, shelter)
		}
	}

	return result, nil
}

// Close the datastore. No-op for now.
func (f *FileDatastore) Close() error {
	return nil
}
