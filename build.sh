#!/bin/bash
#
# Build the Docker image. 
#
IMAGE="netbucket/pet-rescue"
VERSION="0.0.1"

# Build and push the plaintext/HTTP version
docker build . -f Dockerfile -t $IMAGE:$VERSION -t $IMAGE:latest
#docker push $IMAGE_PLAINTEXT:$VERSION
#docker push $IMAGE_PLAINTEXT:latest
