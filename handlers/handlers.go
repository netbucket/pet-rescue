// Package handlers contains HTTP endpoint handler implementations
package handlers

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"net/http/httputil"
	"os"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/netbucket/pet-rescue/datastore"
)

type handlerContext struct {
	Model datastore.Datastore
}

// AssignHandlers assigns handler functions for HTTP endpoints
func AssignHandlers(d datastore.Datastore) http.Handler {
	ctx := handlerContext{d}

	r := mux.NewRouter()

	r.HandleFunc("/shelters/", ctx.sheltersHandler)
	r.HandleFunc("/shelters/{id}", ctx.shelterDetailsHandler)

	// Add request logging and GZIP compression
	var h http.Handler
	{
		h = handlers.LoggingHandler(os.Stdout, r)
		h = requestLoggingHandler(os.Stdout, h)
		h = handlers.CompressHandler(h)
	}

	return h
}

// Send the payload according to OpenAPI conventions
func sendResponse(w http.ResponseWriter, v interface{}) error {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	payload, err := json.Marshal(struct {
		D interface{} `json:"data"`
	}{v})

	if err == nil {
		w.Write(payload)
	}

	return err
}

// Send an error response back
func sendError(w http.ResponseWriter, httpStatus int, err error) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	m := struct {
		Message string `json:"message"`
	}{err.Error()}

	payload, err := json.Marshal(struct {
		D interface{} `json:"error"`
	}{m})

	if err != nil {
		log.Println(err)
		return
	}

	w.WriteHeader(httpStatus)
	w.Write(payload)
}

// Log request body - all of it
func requestLoggingHandler(out io.Writer, h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		h.ServeHTTP(w, r)

		body, err := httputil.DumpRequest(r, true)
		if err == nil {
			out.Write(body)
		}
	})
}
