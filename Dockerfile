# Run a clean build
FROM golang:1.11 as builder

RUN go get github.com/gorilla/handlers
RUN go get github.com/gorilla/mux
RUN go get github.com/netbucket/pet-rescue

WORKDIR /go/src/github.com/netbucket/httpr
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o pet-rescue

# Reduce the image size and add TLS certs
FROM alpine:3.7
RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*

WORKDIR /
COPY --from=builder /go/src/github.com/netbucket/pet-rescue/pet-rescue .

EXPOSE 8080

ENTRYPOINT ["/pet-rescue"]
