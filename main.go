// A pet rescue - backend service.
package main

import (
	"flag"
	"log"
	"net/http"

	"github.com/netbucket/pet-rescue/datastore"
	"github.com/netbucket/pet-rescue/handlers"
)

func main() {
	var (
		httpService = flag.String(
			"http", ":8080", " HTTP service address, e.g. ':8080'")
		data = flag.String(
			"data", "shelters.json", "Datastore configuration, e.g. file path")
	)

	flag.Parse()

	ds := new(datastore.FileDatastore)

	err := ds.Open(*data)

	if err != nil {
		log.Fatal(err)
	}

	defer ds.Close()

	r := handlers.AssignHandlers(ds)

	log.Fatalln(http.ListenAndServe(*httpService, r))
}
