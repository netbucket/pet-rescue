package handlers

import (
	"errors"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
	"strings"
)

// sheltersHandler produces the listing of all available shelters
func (ctx *handlerContext) sheltersHandler(w http.ResponseWriter, r *http.Request) {
	data, err := ctx.Model.ListShelters()

	if err != nil {
		sendError(w, http.StatusInternalServerError, err)
		return
	}

	sendResponse(w, data)
}

// shelterDetailsHandler provides shelter details. In addition to supporting
// a single shelter via the /shelters/{id} URI, batching is also supported.
// That is, the API caller may pass multiple ID values separated by a comma:
// /shelters/{id1},{id2}...
func (ctx *handlerContext) shelterDetailsHandler(w http.ResponseWriter, r *http.Request) {
	IDs, err := parseIDList(mux.Vars(r)["id"])

	if err != nil {
		sendError(w, http.StatusBadRequest, err)
		return
	}

	switch len(IDs) {
	case 1:
		// This is a non-batching request for a single shelter
		data, exists, modelErr := ctx.Model.FindShelter(IDs[0])

		if modelErr != nil {
			sendError(w, http.StatusInternalServerError, modelErr)
			return
		}

		if exists {
			sendResponse(w, data)
		} else {
			sendError(w, http.StatusNotFound, errors.New("Not found"))
		}

	default:
		// This is a batching request for multiple shelters
		data, modelErr := ctx.Model.FindShelters(IDs)

		if modelErr != nil {
			sendError(w, http.StatusInternalServerError, modelErr)
			return
		}

		if len(data) == 0 {
			sendError(w, http.StatusNotFound, errors.New("Not found"))
		} else {
			sendResponse(w, data)
		}
	}
}

// Given the value of the {id} parameter can contain a single or multiple,
// comma separated IDs, attempt to split the parameter into a slice of integers
func parseIDList(param string) ([]int, error) {
	rawIDs := strings.Split(param, ",")

	IDs := make([]int, len(rawIDs))

	for idx, rawID := range rawIDs {
		ID, err := strconv.Atoi(rawID)

		if err != nil {
			return nil, err
		}

		IDs[idx] = ID
	}

	return IDs, nil
}
