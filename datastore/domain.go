package datastore

// ShelterSummary contains high level shelter info
type ShelterSummary struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// Shelter contains shelter details
type Shelter struct {
	ShelterSummary
	Capacity int     `json:"capacity"`
	Address  Address `json:"address"`
}

// Address is a street address
type Address struct {
	Street string `json:"street_address"`
	City   string `json:"city"`
	State  string `json:"state"`
	Zip    string `json:"zip_code"`
}
