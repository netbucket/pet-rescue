package health

import (
	"fmt"

	"github.com/netbucket/pet-rescue/datastore"
)

type Health struct {
	Healthy bool   `json:"healthy"`
	Message string `json:"message"`
}

func CheckHealth() Health {
	var result Health

	dataEntries := len(datastore.ListShelters())

	result.Healthy = (dataEntries > 0)

	if result.Healthy {
		result.Message = fmt.Sprintf("Data store sane: %d entries", dataEntries)
	} else {
		result.Message = "Data store is empty"
	}

	return result
}
